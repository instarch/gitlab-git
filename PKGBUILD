# Maintainer: Josh VanderLinden <arch@cloudlery.com>
pkgname=gitlab
pkgver=20121214
pkgrel=1
pkgdesc="Fast, secure and stable solution based on Rails & Gitolite."
arch=('any')
url="https://bitbucket.org/instarch/gitlab-git/"
license=('MIT')
depends=(
  'ruby>=1.9.3'
  'mysql'
  'git'
  'redis'
  'nginx'
  'ruby-unicorn'
  'sudo'
  'python-pygments'
)
makedepends=('rubygems' 'pkgtools' 'ruby-charlock_holmes' 'ruby-bundler')
provides=('gitlab')
install=gitlab.install

source=(
  "https://raw.github.com/gitlabhq/gitlab-recipes/master/nginx/gitlab"
  "gitlab.install"
)

md5sums=('69f907465eb663072bc1e8911ae795d1'
         '30b5e098bfb069446e913656315f1c47')

_gitroot=https://github.com/gitlabhq/gitlabhq.git
_gitname=gitlab-repo

_gitoliteroot=https://github.com/gitlabhq/gitolite.git
_gitolitename=gitolite

build() {
  cd "$srcdir"
  msg "Cloning GitLab...."

  if [[ -d "$_gitname" ]]; then
    cd "$_gitname" && git pull origin stable
    msg "The local files are updated."
    cd ..
  else
    git clone -b stable "$_gitroot" "$_gitname"
  fi
  msg "GitLab checkout done or server timeout"

  msg "Cloning the GitLab fork of Gitolite..."
  if [[ -d "$_gitolitename" ]]; then
    cd "$_gitolitename" && git pull origin gl-v304
    msg "The local files are updated."
    cd ..
  else
    git clone -b gl-v304 "$_gitoliteroot" "$_gitolitename"
  fi
  msg "GitLab fork of Gitolite checkout done or server timeout"

  rm -rf "$srcdir/$_gitname-build" "$srcdir/$_gitolitename-build"
  git clone "$srcdir/$_gitname" "$srcdir/$_gitname-build"
  git clone "$srcdir/$_gitolitename" "$srcdir/$_gitolitename-build"
}

package() {
  install -d "${pkgdir}/etc/nginx/sites-available"
  install -d "${pkgdir}/etc/nginx/sites-enabled"
  install -d "${pkgdir}/home/gitlab/gitlab"
  install -d "${pkgdir}/home/git/bin"
  install -d "${pkgdir}/home/git/gitolite"
  install -d "${pkgdir}/home/git/repositories"
  install -d "${pkgdir}/home/git/.gitolite/hooks/common"

  install -D $_gitname/LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

  msg "Copying files..."
  cp -a $srcdir/$_gitname-build/* "$pkgdir/home/gitlab/gitlab/"
  cp -a $srcdir/$_gitolitename-build/* "$pkgdir/home/git/gitolite/"

  cd "${pkgdir}/home/gitlab"

  msg "Copying configuration files..."
  cp "${pkgdir}"/home/gitlab/gitlab/config/gitlab.yml{.example,}
  cp "${pkgdir}"/home/gitlab/gitlab/config/database.yml{.mysql,}
  cp "${pkgdir}"/home/gitlab/gitlab/config/unicorn.rb{.example,}

  install -D "${pkgdir}/home/gitlab/gitlab/lib/hooks/post-receive" "${pkgdir}/home/git/.gitolite/hooks/common/post-receive"
  install -D "$srcdir/gitlab" "${pkgdir}/etc/nginx/sites-available/"
  ln -s /etc/nginx/sites-available/gitlab "${pkgdir}/etc/nginx/sites-enabled/gitlab"
}

# vim:set ts=2 sw=2 et:
